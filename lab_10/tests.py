from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *

# Create your tests here.
class Lab10UnitTest(TestCase):

	def test_lab_10_url_is_exist(self):
		response = Client().get('/lab-10/')
		self.assertEqual(response.status_code, 200)

	def test_lab10_using_index_func(self):
		found = resolve('/lab-10/')
		self.assertEqual(found.func, index)
