var themes = `[
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":0,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":0,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":0,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":0,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":0,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":0,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":0,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":0,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":0,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":0,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
]`;

var themesDemo = [
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":0,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":0,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":0,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":0,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":0,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":0,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":0,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":0,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":0,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":0,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
];

var selectedThemes = '{"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}}';

localStorage.setItem("themes",themes);
localStorage.setItem("selectedThemes",selectedThemes);

$(document).ready(function() {
//    $('.my-select').select2({'data': JSON.parse(localStorage.getItem("themes"))});
    $('.my-select').select2({'data': themesDemo)});
});

//Theme Change
$('.apply-button').on('click', function () {
    var changedTo = $('.my-select option:selected').text();
    var length = JSON.parse(localStorage.getItem("themes")).length;

    var bg = "";
    var font = "";

    for (var index = 0; index < length; index++) {
        if(JSON.parse(localStorage.getItem("themes"))[index]['text'] === changedTo){
            bg = JSON.parse(localStorage.getItem("themes"))[index]["bcgColor"];
            font = JSON.parse(localStorage.getItem("themes"))[index]["fontColor"];
            localStorage.setItem("selectedThemes", JSON.parse(localStorage.getItem("themes"))[index]);
        }
    }

    document.body.style.backgroundColor = bg;
    document.body.style.color = font;
});

	

	
// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    /* implemetnasi clear all */
	print.value = "";
    erase = false;
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}

//fungsi chat
$(".chat-text").keypress(function(chat) {
    //cek apakah key yang ditekan tombol Enter atau bukan
    if(chat.which == 13) {
    	chat.preventDefault(); //default event tombol enter tidak dilakukan
    	var input = $("textarea").val(); //ambil data di textarea
		if(input !== ""){
		    $("textarea").val(""); //menghapus text area
			$(".msg-insert").append('<p class="msg-send">'+input+'</p>'+'<br/>'); //munculkan text di body chat
		}
		

    }
});



// END
