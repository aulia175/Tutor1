from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, HttpResponseBadRequest
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers

from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
import os
import json

response = {}
csui_helper = CSUIhelper()

def index(request):
    # Page halaman menampilkan list mahasiswa yang ada
    # TODO berikan akses token dari backend dengan menggunakaan helper yang ada
    
	
    page = request.GET.get('page',1)
    resp = csui_helper.instance.get_list_at(page)
    mahasiswa_list = resp['results']

    friend_list = Friend.objects.all()
    response = {"current_page": page, "mahasiswa_list": mahasiswa_list, "friend_list": friend_list, 'author' : "Mohammad Aulia Hafidh"}
    html = 'lab_7/lab_7.html'
    return render(request, html, response)

def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)

def get_friend_list(request):
	return JsonResponse(dict(friend_list=list(Friend.objects.values())), content_type='application/json');
	
@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        isExist = Friend.objects.filter(npm=npm).exists()

        if not isExist:
            friend = Friend(friend_name=name, npm=npm)
            friend.save()
            data = model_to_dict(friend)
            return JsonResponse({'status':True, 'data':data})
        else:
            return HttpResponse({'status':False})

@csrf_exempt
def delete_friend(request):
    status = False
    if request.method == 'POST':
        friend_id = request.POST['id']
        Friend.objects.filter(id=friend_id).delete()
        status = True
    return JsonResponse({'status':status})

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    data = {
        'is_taken': Friend.objects.filter(npm__iexact=npm).exists()
    }
    return JsonResponse(data)

def model_to_dict(obj):
    data = serializers.serialize('json', [obj,])
    struct = json.loads(data)
    data = json.dumps(struct[0]["fields"])
    return data

@csrf_exempt
def friend_detail(request):
    if request.method == 'GET':
        
        friend_id = request.GET.get('id', None)
        if (friend_id == None):
            return HttpResponseRedirect('/lab-7/friend-list/')
        
        friend = Friend.objects.filter(id=friend_id)
        if (len(friend) == 0):
            return HttpResponseRedirect('/lab-7/friend-list/')
        
        fr = friend[0]
        data_get = csui_helper.instance.get_data_mahasiswa(fr.npm)
        response['detail'] = data_get
        html = 'lab_7/detail_teman.html'
        return render(request, html, response)
        
@csrf_exempt
def get_list_at_page(request):
    if request.method == 'POST':
        page = request.POST['page']
        listAtPage = csui_helper.instance.get_list_at(page)['results']
        return JsonResponse({'listOfMhs': json.dumps(listAtPage)})
